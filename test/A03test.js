// This file tests our application code - open this file in a browser to see the test results. 

// It uses QUnit, a unit testing library for JavaScript. 

// Unit testing is a common, professional practice. 

// It helps us verify our code is correct in all cases.

// To complete this assignment, go to the end of this file, and uncomment the last 5 tests. 

// Refresh.  You will see several failing tests. 

// Modify the code in your appplication (M03.js) to get these tests to pass. 

// References: 
// https://qunitjs.com/
// https://www.sitepoint.com/getting-started-qunit/
// http://jsbin.com/tusuvixi/1/edit?html,js,output

QUnit.test('Testing the new calculateMonthlyPayment function with 3 sets of inputs', function (assert) {
    assert.equal(calculateMonthlyPayment(12,12,12), 	
    157.0909090909091, "works with a 3 different number");
    assert.equal(calculateMonthlyPayment(-3,12,11), -36, "works with negative input");
    assert.equal(calculateMonthlyPayment(0,0,0), 0, "works with zero number");
 });






