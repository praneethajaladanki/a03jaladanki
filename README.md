# A03 

personal website using node.js, express, and socket.io

## How to use

Open a command window in your c:\44563\A03Jaladanki folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node server.js

```
